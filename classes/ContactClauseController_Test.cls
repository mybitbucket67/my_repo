@isTest
private class ContactClauseController_Test {
    @isTest
    private static void testGetData(){
        Account aobj= new Account();
        aobj.Name='Sample';
        insert aobj;
        Contract contractObject= new Contract();
        contractObject.Name='Test contract';
        contractObject.AccountId=aobj.id;
        
        insert contractObject;
        
        Contract_Clause__c contract_ClausesObject= new Contract_Clause__c();
        contract_ClausesObject.Name='Test contract';
        contract_ClausesObject.Type__c='Other';
        
        insert contract_ClausesObject;
        
        Contract_Drafting__c contractDraftingObject= new Contract_Drafting__c();
        contractDraftingObject.Name='Test contract';
        contractDraftingObject.contract__c=contractObject.id;
        contractDraftingObject.Contract_Clause__c=contract_ClausesObject.id;
        insert contractDraftingObject;
        ContactClauseController.getData(contractDraftingObject.id);
          ContactClauseController.deleteData(contractDraftingObject.id);
       
    }

}