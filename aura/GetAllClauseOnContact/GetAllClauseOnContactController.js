({
    getDataFun: function(component,event,helper){
        helper.getDataHelper(component,event,helper);
    },
    createData:function(component,event,helper){
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Contract_Drafting__c"
            
        });
        createRecordEvent.fire();
    },
    deleteFun: function(component,event,helper){
        
         var dataId=event.getSource().get("v.name");

        helper.deleteDataHelper(component,event,helper,dataId);
    }
})