({
    getDataHelper: function(component,event,helper){
        var action = component.get("c.getData");

        action.setParams({'recordId':component.get("v.recordId")});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var data=response.getReturnValue();
                component.set("v.clauseList",data );
                component.set("v.count",data.length );
            }
        });
        $A.enqueueAction(action);
    },
    deleteDataHelper: function(component,event,helper,dataId){
        self=this;
        var action = component.get("c.deleteData");

       
        
        action.setParams({'dataId':dataId});
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var resultsToast = $A.get("e.force:showToast");
                resultsToast.setParams({
                    "title": "Delete!",
                    "message": "The record was deleted.",
                    "type":'success'
                });
                resultsToast.fire();
            }
        });
        $A.enqueueAction(action);
        self.getDataHelper(component,event,helper);
    },
})