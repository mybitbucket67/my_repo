public class ContactClauseController {
    @AuraEnabled
    public static List<Contract_Drafting__c> getData(String recordId){
        List<Contract_Drafting__c>contractList=[select Id ,Name ,Contract_clause__r.Id,Contract_clause__r.Name,Contract_clause__r.Description__C,Contract_clause__r.Type__c from Contract_drafting__c  where contract__c=:recordId];

           return contractList;
      
    }
    @AuraEnabled
    public static void deleteData(String dataId){
        system.debug('recordId'+dataId);
        if(dataId!=null){
           Contract_Drafting__c draftObject= new Contract_Drafting__c();
            draftObject.id=dataId;
            delete draftObject;
        }
    }

}